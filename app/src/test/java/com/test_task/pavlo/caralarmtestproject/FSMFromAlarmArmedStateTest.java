package com.test_task.pavlo.caralarmtestproject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Pavlo on 23.02.2018.
 * Commited on 24.02.2018
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class FSMFromAlarmArmedStateTest {
    AlarmManagementActivity activity;

    @Before
    public void setVariables() {
        activity = Robolectric.setupActivity(AlarmManagementActivity.class);
        //Is already AlarmArmed
    }

    @Test
    public void singleLockDoesNotChangeThisState() throws Exception {
        activity.lockButton.performClick();
        assertEquals(activity.stateView.getText().toString(), "AlarmArmed_AllLocked");

        //reset state
        activity.wLockButton.performClick();
    }

    @Test
    public void singleUnlockChangesStateToDriverUnlocked() throws Exception {
        activity.unlockButton.performClick();
        assertEquals(activity.stateView.getText().toString(), "AlarmDisarmed_DriverUnlocked");

        //reset state
        activity.wLockButton.performClick();
    }

    @Test
    public void doubleUnlockChangesStateToAllUnlocked() throws Exception {
        activity.wUnlockButton.performClick();
        assertEquals(activity.stateView.getText().toString(), "AlarmDisarmed_AllUnlocked");

        //reset state
        activity.wLockButton.performClick();
    }

    @Test
    public void doubleLockDoesNotChangeThisState() throws Exception {
        activity.wLockButton.performClick();
        assertEquals(activity.stateView.getText().toString(), "AlarmArmed_AllLocked");

        //reset state
        activity.wLockButton.performClick();
    }
}
