package com.test_task.pavlo.caralarmtestproject;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;


import static org.junit.Assert.*;

/**
 * Created by Pavlo on 23.02.2018.
 * Commited on 24.02.2018
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)

public class AlarmManagementActivityTest {

    //Variables
    AlarmManagementActivity activity;

    @Before
    public void setVariables() {
        activity = Robolectric.setupActivity(AlarmManagementActivity.class);
    }

    @Test
    public void initialStateCheck() throws Exception {
        String INITIAL_STATE = "AlarmArmed_AllLocked";
        int INITIAL_COLOR = Color.RED;

        assertEquals(activity.alarmState.getState(), INITIAL_STATE);
        assertEquals(activity.stateView.getText().toString(), INITIAL_STATE);
        assertEquals(((ColorDrawable)activity.stateView.getBackground()).getColor(),INITIAL_COLOR);
    }

    @Test
    public void stateChangeCheck() throws Exception {
        String INITIAL_STATE = "AlarmArmed_AllLocked";
        int INITIAL_COLOR = Color.RED;

        activity.unlockButton.performClick();

        assertNotEquals(activity.stateView.getText().toString() , INITIAL_STATE);
        //assertNotEquals(((ColorDrawable)activity.stateView.getBackground()).getColor()
        //        ,INITIAL_COLOR);

        //to reset to initial state
        activity.wLockButton.performClick();
        assertEquals(activity.alarmState.getState(), INITIAL_STATE);
        //assertEquals(activity.stateView.getText().toString(), INITIAL_STATE);
        //assertEquals(((ColorDrawable)activity.stateView.getBackground()).getColor(),INITIAL_COLOR);
    }

    @Test
    public void ColorIsOnlyRedForAlarmArmedState() throws Exception {
        String INITIAL_STATE = "AlarmArmed_AllLocked";
        int INITIAL_COLOR = Color.RED;
        assertEquals(activity.alarmState.getState(), INITIAL_STATE);
        assertEquals(((ColorDrawable)activity.stateView.getBackground()).getColor(),INITIAL_COLOR);

        activity.unlockButton.performClick(); //Should be "DriverUnlocked". refer to other tests

        assertNotEquals(activity.stateView.getText().toString() , INITIAL_STATE);
        assertNotEquals(((ColorDrawable)activity.stateView.getBackground()).getColor()
                ,INITIAL_COLOR);

        activity.unlockButton.performClick(); //Should be "AllUnlocked", refer to other tests

        assertNotEquals(activity.stateView.getText().toString() , INITIAL_STATE);
        assertNotEquals(((ColorDrawable)activity.stateView.getBackground()).getColor()
                ,INITIAL_COLOR);

        activity.lockButton.performClick(); //Should be "AllLocked", refer to other tests

        assertNotEquals(activity.stateView.getText().toString() , INITIAL_STATE);
        assertNotEquals(((ColorDrawable)activity.stateView.getBackground()).getColor()
                ,INITIAL_COLOR);

        activity.wLockButton.performClick(); //Should be "AlarmArmed", refer to other tests

        assertEquals(activity.stateView.getText().toString() , INITIAL_STATE);
        assertEquals(((ColorDrawable)activity.stateView.getBackground()).getColor()
                ,INITIAL_COLOR);
    }
}