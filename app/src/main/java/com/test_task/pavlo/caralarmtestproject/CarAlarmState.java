package com.test_task.pavlo.caralarmtestproject;

/**
 * Created by Pavlo on 22.02.2018.
 */

public class CarAlarmState {
    private boolean locked;
    private boolean driverLocked;
    private boolean alarmArmed;
    private static CarAlarmState state;

    private CarAlarmState() {
        locked = true;
        alarmArmed = true;
        driverLocked = true;
    }

    public static CarAlarmState getInstance() {
        if (state == null) {
            synchronized (CarAlarmState.class) {
                if (state == null)
                    state = new CarAlarmState();
            }
        }
        return state;
    }

    public String getState() {
        String result = alarmArmed? "AlarmArmed_AllLocked" :
                driverLocked && locked? "AlarmDisarmed_AllLocked" :
                        !locked ? "AlarmDisarmed_AllUnlocked" :
        /*!driverLocked, but locked*/        "AlarmDisarmed_DriverUnlocked";
        return(result);
    };

    public void singleLock() {
        driverLocked = true;
        locked = true;
    }

    public void doubleLock() {
        driverLocked = true;
        locked = true;
        alarmArmed = true;
    }

    public void singleUnlock() {
        alarmArmed = false;
        if(driverLocked) {
            driverLocked = false;
        } else {
            driverLocked = false;
            locked = false;
        }
    }

    public void doubleUnlock() {
        alarmArmed = false;
        locked = false;
        driverLocked = false;
    }
}
