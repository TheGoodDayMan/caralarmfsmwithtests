package com.test_task.pavlo.caralarmtestproject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//THis comment is here to commit into an online repository, yaay!
public class AlarmManagementActivity extends AppCompatActivity {

    CarAlarmState alarmState = CarAlarmState.getInstance();
    Button unlockButton, wLockButton, lockButton, wUnlockButton;
    TextView stateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_management);

        //find stateView
        stateView = (TextView) findViewById(R.id.stateView);

        //find buttons
         unlockButton = (Button) findViewById(R.id.singleUnlockButton);
         wUnlockButton = (Button) findViewById(R.id.doubleUnlockButton); //w = double
         lockButton = (Button) findViewById(R.id.singleLockButton);
         wLockButton = (Button) findViewById(R.id.doubleLockButton);

        //button actions
        unlockButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alarmState.singleUnlock();
                stateView.setText(alarmState.getState());
                setSVcolor();
            }
        });

        wUnlockButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alarmState.doubleUnlock();
                stateView.setText(alarmState.getState());
                setSVcolor();
            }
        });

        lockButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alarmState.singleLock();
                stateView.setText(alarmState.getState());
                setSVcolor();
            }
        });

        wLockButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alarmState.doubleLock();
                stateView.setText(alarmState.getState());
                setSVcolor();
            }
        });




        stateView.setText(alarmState.getState());
        setSVcolor();
    }

    public void setSVcolor() {
        TextView stateView = (TextView) findViewById(R.id.stateView);
        if (stateView.getText().toString().contains("AlarmArmed")) {
            //stateView.setBackgroundResource(R.color.solid_red);
            stateView.setBackgroundColor(Color.RED);
        } else {
            stateView.setBackgroundColor(Color.GREEN);
        }
    }
}
